import java.util.ArrayList;
import java.util.Random;

/**
 * Write a description of class Pb_Balakrishnan_Akash_Permutations here.
 *
 * @author (Akash Balakrishnan)
 * hours 3 
 * when calling nextPermutation() method
 * Picked random index from 0 to 9 
 * swapped the arraylist twice 
 * @version (a version number or a date)
 */
public class P6_Balakrishnan_Akash_Permutations
{
    // instance variables - replace the example below with your own
    private int low = 1;
    private int high = 10;
    
    public static void main(String[] args) {
        
        P6_Balakrishnan_Akash_Permutations pbak = new P6_Balakrishnan_Akash_Permutations();
        int i = 0;
        while (i < 10) {
            i++;
            System.out.print("\n List "+i+" : ");
            pbak.nextPermutation();
        }
    }

    // Returns an ArrayList of Integers that are a permutation of the numbers 1-10

    public ArrayList<Integer> nextPermutation() {
        ArrayList<Integer>  arrlist = defaultArrayList();
        
        int index = getRandomIndex();
        Integer val = arrlist.get(index);
        arrlist.remove(index);
        arrlist.add(0, val);
        
        index = getRandomIndex();
        val = arrlist.get(index);
        arrlist.remove(index);
        arrlist.add(high-1, val);
        
        index = getRandomIndex();
        val = arrlist.get(index);
        arrlist.remove(index);
        arrlist.add(high-1, val);
        
        index = getRandomIndex();
        val = arrlist.get(index);
        arrlist.remove(index);
        arrlist.add(0, val);
        
        System.out.println(arrlist.toString());
        return arrlist;
    }
 
    public ArrayList<Integer> defaultArrayList() {
        ArrayList<Integer>  arrlist = new ArrayList<Integer>();
        for (int i = low; i <= high ; i++) {
            arrlist.add(i);
        }
        
        return arrlist;
    }
    
    public int getRandomIndex(int i) {
         Random randomno = new Random();
         int val = randomno.nextInt(high-low);
         //System.out.println("Random Value "+val);
         return val;
    }
    
    
    public int getRandomIndex() {
   
    Random generator = new Random(); 
    int randomNum = generator.nextInt((high - low) + 1);
    //System.out.print("Value "+randomNum+ " ");
    if (randomNum < 0) {
        randomNum =  randomNum + generator.nextInt(high - low);
    }
    if (randomNum > 9) {
        randomNum =  randomNum - generator.nextInt(high - low);
    }
    //System.out.print("Value "+randomNum+ " ");

    return randomNum;
   }
}
