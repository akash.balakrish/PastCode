
import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.*;
/**
 * Write a description of class p6_Balakrishnan_Akash_16_2 here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class p6_Balakrishnan_Akash_16_2
{
    // instance variables - replace the example below with your own
    private int x;

    public static void main(String[] args) {
      Scanner in;
      ArrayList<Integer> arrayList  = new ArrayList<Integer>();
      int index = 0;
      String str = "";
     try{
         in = new Scanner(new File("compact.txt"));
         str = str.concat(in.nextLine());
     
        }catch(IOException i){
            System.out.println("Error: " + i.getMessage());
        }
        
         String arrStr[] = str.split(" ");
         int arr[] = new int[arrStr.length];
         for (int j = 0; j < arrStr.length ; j++) {
             String string = arrStr[j].trim();
             if (string.length() > 0) {
                 int num = Integer.parseInt(string);
                 System.out.println("Num ..."+num);
                 arr[index] = num;
                 arrayList.add(num);
                 index++;
                }
         }
         
        compactArray(arr);
        compactArrayList(arrayList);
    }
    
   

    /**
     * An example of a method - replace this comment with your own
     *
     * @param  y  a sample parameter for a method
     * @return    the sum of x and y
     */
    public static  void compactArray(int[] arr){
        System.out.println("Array");
        System.out.println("Before "+Arrays.toString(arr));
        // put your code here
        int numindex = -1;
        for (int i = 0 ; i < arr.length; i++) {
            if ( arr[i] != 0) {
                numindex++;
                if (i != numindex) {
                    arr[numindex] = arr[i];
                    arr[i] = 0;
                }
            }
        }
        System.out.println(Arrays.toString(arr));

        
    }
    
    public static void  compactArrayList(ArrayList<Integer> arr) {
        System.out.println("ArrayList");
        System.out.println("Before : "+ arr);
        Iterator itr = arr.iterator(); 
        while (itr.hasNext()) 
        { 
            int x = (Integer)itr.next(); 
            if (x == 0) 
                itr.remove(); 
        }
        
        System.out.println("After : "+ arr);
    }
}
