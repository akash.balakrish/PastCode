import java.util.*;

public class Deck
{
    /* *** TO BE IMPLEMENTED IN ACTIVITY 3 *** */
    
    List<Card> cards;
    Card card;
    
    public Deck() {
        cards = new ArrayList<Card>();
    }
    
    public void add(Card card){
        this.card = card;
        cards.add(0,card);
    }
    
   
    public void shuffle() {
        Random r = new Random();
        int val = cards.size();
        Card temp = null;
        for (int i = 0 ; i < val ; i++) {
            int j = r.nextInt(12)+1;
            temp = cards.get(i);
            cards.set(i, cards.get(j));
            cards.set(j, temp);
        }
        
    }
    
    public  int getSize() {
        return cards.size();
    }
    
    public Card getTopStack(int i) {
        card =  cards.get(i);
        return card;
    }
    
     @Override
    public String toString() {
        /* *** TO BE IMPLEMENTED IN ACTIVITY 2 *** */
        return "Card [symbol=" + this.card.getSymbol() + ", size=" + this.card.getValue() + "]";

    }
    
}
