public class DeckTester
{
    public static void main(String[] args) {
        /* *** TO BE IMPLEMENTED IN ACTIVITY 3 *** */
        
        String[] symbols = {"A", "2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K"};
        int[] values = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};

        Deck deck = new Deck();
        
    for (int i = 0 ; i < 13 ; i++) {
        deck.add(new Card(symbols[i], values[i]));
        System.out.println(deck.toString());
    }
    
    System.out.println("------------------Last one come first------------------");
    int i = 0;
    while ( i < 13) {
        System.out.println(deck.getTopStack(i).toString());
        i++;
    }
    
    System.out.println("------------------Shuffle------------------");
    deck.shuffle();

     i = 0;
    while ( i < 13) {
        System.out.println(deck.getTopStack(i).toString());
        i++;
    }
    
    }
}
