
public class exceptionPractice {

	public static void main(String[] args) {

      exceptionPractice ep = new exceptionPractice();
      ep.printReciprocals(0, 10);
      //ep.lengthTest(null);


      double side1 = -1;
 	  double side2 = - 2;
	  double ans = ep.area(side1, side2);
	  System.out.printf("The area of a %f by %f rectangle is %.2f",side1, side2, ans);

	}

	/*public void printReciprocals(int lowerBound, int upperBound) {
		for (int i = lowerBound; i <= upperBound; i++) {
 			System.out.println("1/" + i + " = " + (1.0/i));
		}
	}*/

	public void printReciprocals(int lowerBound, int upperBound) {
	try{
			for (int i = lowerBound; i <= upperBound; i++) {
	 			System.out.println("1/" + i + " = " + (1.0/i));
			}
			System.out.println(1/0);


		} catch(ArithmeticException e) {
			System.out.println("Error: Division by zero");
		}
	}


	public int lengthTest(String a) throws NullPointerException {
		if (a == null){ 
  		throw new NullPointerException("String is null");
		}
  		return a.length();
  	}
	
 		
 	public static double area(double length, double width) {
 		if ((length < 0) || ( width < 0) ){
      	throw new IllegalArgumentException("Negative values not permitted in area() method");
   		}
 		return length * width;
 	}


 }    


 /*


 1/0 = Infinity
1/1 = 1.0
1/2 = 0.5
1/3 = 0.3333333333333333
1/4 = 0.25
1/5 = 0.2
1/6 = 0.16666666666666666
1/7 = 0.14285714285714285
1/8 = 0.125
1/9 = 0.1111111111111111
1/10 = 0.1
Error: Division by zero
Exception in thread "main" java.lang.NullPointerException
	at exceptionPractice.lengthTest(exceptionPractice.java:34)
	at exceptionPractice.main(exceptionPractice.java:8)

Exception in thread "main" java.lang.NullPointerException: String is null
	at exceptionPractice.lengthTest(exceptionPractice.java:35)
	at exceptionPractice.main(exceptionPractice.java:8)

Exception in thread "main" java.lang.IllegalArgumentException: Negative values not permitted in area() method
	at exceptionPractice.area(exceptionPractice.java:48)
	at exceptionPractice.main(exceptionPractice.java:13)
	*/


 