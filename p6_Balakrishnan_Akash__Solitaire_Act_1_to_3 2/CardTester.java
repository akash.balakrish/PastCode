public class CardTester
{
    public static void main(String[] args) {
        /* *** TO BE IMPLEMENTED IN ACTIVITY 2 *** */
        Card c1 = new Card("A",1);
        Card c2 = new Card("Ten",10);
        System.out.println("1 and 10 :" +c1.equals(c2)+"\n");
        
        c1 = new Card("Five",5);
        c2 = new Card("Ten",10);
        System.out.println("5 and 10 :"+c1.equals(c2)+"\n");
        
        
        c1 = new Card("A",10);
        c2 = new Card("Ten",10);
        System.out.println("10 and 10 :"+c1.equals(c2)+"\n");
        
    }
}
