import java.util.*;
/**
 * Akash Balakrishnan
 * 3 hours
 * For facedown there I took the 0th element of the shuffled deck
 * for the 7 seven stacks
 * The Draw pile was also made the sameway by taking the 0th element fo the 
 * shuffeld deck.
 * For activity 5 we checked if the draw pile is empty by just checking 
 * if array length is 0 for the deck and the face down piles.
 * 
 * 
 */
public class Board
{   
    /* *** TO BE IMPLEMENTED IN ACTIVITY 4 *** */
    // Attributes
    private int numStacks;
    private int numDecks;
    private int totalCards;
    Deck cardPile;
    ArrayList cardPileList;
    ArrayList<Deck> stacks;
    private int suits;
    /**
     *  Sets up the Board and fills the stacks and draw pile from a Deck
     *  consisting of numDecks Decks.  The number of Cards in a Deck
     *  depends on the number of suits. Here are examples:
     *  
     *  # suits     # numDecks      #cards in overall Deck
     *      1            1          13 (all same suit)
     *      1            2          26 (all same suit)
     *      2            1          26 (one of each suit)
     *      2            2          52 (two of each suit)
     *      4            2          104 (two of each suit)
     *      
     *  Once the overall Deck is built, it is shuffled and half the cards
     *  are placed as evenly as possible into the stacks.  The other half
     *  of the cards remain in the draw pile.  If you'd like to specify
     *  more than one suit, feel free to add to the parameter list.
     */    
    public Board(int numStacks, int numDecks) {
        /* *** TO BE IMPLEMENTED IN ACTIVITY 4 *** */
        this.numStacks = numStacks;
        this.numDecks = numDecks;
        this.totalCards = numDecks * 13;
        
        String[] symbols = {"A", "2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K"};
        int[] values = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};

        Deck deck = new Deck();
        for (int j = 0 ; j < numDecks ; j++) {
            for (int i = 0 ; i < 13 ; i++) {
                deck.add(new Card(symbols[i], values[i]));
                System.out.println(deck.toString());
            }
        }
        
        deck.shuffle();
        
        System.out.println("........Draw Pile.........");
        cardPile = new Deck();
        Card card = null;
        int end = this.totalCards/2;
        
        for (int i = 0 ; i < end ;i++) {
            card = deck.getTopStack(i);
            cardPile.add(deck.getTopStack(i));
            //System.out.println(card.toString());
        }
        
        
        System.out.println("........Stack Pile.........");       
        stacks  = new ArrayList<Deck>();
        int eachstackCount = (this.totalCards/2 ) / numStacks; 
        System.out.println("Number in each stack "+eachstackCount);
        int count = end;
        for (int i = 0 ; i < numStacks ;i++) {
          Deck newStack = new Deck();
          int j = 0;
  
          while ((j < eachstackCount) && (count < this.totalCards))  {
              newStack.add(deck.getTopStack(count));
              count++;
              j++;
          }
          stacks.add(newStack);
        }
        
        /* Card pile os empty test */
        
        /*cardPile = new Deck();*/
        
        /* To test empty stacks */
        
        /*stacks  = new ArrayList<Deck>();
        for (int i = 0 ; i < numStacks ;i++) {
    
              Deck newStack = new Deck();
              stacks.add(newStack);
            
        }*/
        /*
          Stack 1: [A, T, 9, 8, 7]   // Note: "T" represents a 10 card
          Stack 2: [J, 8, 6, 5, 4]
          Stack 3: [6, 5, 4, Q, J]
S   tack 4: [K, A, 7, 3, 9] 
           */
        
        System.out.println(isEmpty());
        
        stacks  = new ArrayList<Deck>();
        eachstackCount = 5; 
        System.out.println("Number in each stack "+eachstackCount);
        numStacks = 4;
        Deck newStack = new Deck();
        newStack.add(new Card("A", 1));
        newStack.add(new Card("T", 10));  
        newStack.add(new Card("9", 9));
        newStack.add(new Card("8", 8));
        newStack.add(new Card("7", 7)); 
        stacks.add(newStack);
        
        newStack = new Deck();
        newStack.add(new Card("J", 11));
        newStack.add(new Card("8", 8));  
        newStack.add(new Card("6", 6));
        newStack.add(new Card("5", 5));
        newStack.add(new Card("4", 4)); 
        stacks.add(newStack);
        
        newStack = new Deck();
        newStack.add(new Card("6", 6));
        newStack.add(new Card("5", 5));
        newStack.add(new Card("4", 4));  
        newStack.add(new Card("Q", 12));
        newStack.add(new Card("J", 11)); 
        stacks.add(newStack);
        
        newStack = new Deck();
        newStack.add(new Card("K", 13));
        newStack.add(new Card("A", 1));
        newStack.add(new Card("7", 7));  
        newStack.add(new Card("3", 3));
        newStack.add(new Card("9", 9)); 
        stacks.add(newStack);
    
    }

    /**
     *  Moves a run of cards from src to dest (if possible) and flips the
     *  next card if one is available.  Change the parameter list to match
     *  your implementation of Card if you need to.
     */
    public void makeMove(String symbol, int src, int dest) {
        /* *** TO BE IMPLEMENTED IN ACTIVITY 5 *** */
        Deck decksrc = stacks.get(src);
        Boolean valid = false;
        
        Card cardsrc = null;
       
        ArrayList<Card> arrlist = new ArrayList<Card>();
        int count = 0;
        int val = 0;
            for (int i = 0 ; i < decksrc.getSize() ;i++) {
                cardsrc = decksrc.getTopStack(i);
                if (cardsrc != null) {
                    if (count == 0 ) val = cardsrc.getValue();
                    else {
                        if ((val + 1) != cardsrc.getValue()) {
                            valid = false;
                            System.out.println("Illegal Move");
                            break;
                        }
                        val = cardsrc.getValue();
                    }
                }
                
                arrlist.add(0,cardsrc);
                valid = cardsrc.getSymbol().equals(symbol);
                System.out.println(cardsrc.getSymbol().equals(symbol)); 
                count++;
                if (valid) break;
            }
            
     
        
        if (!valid) 
                System.out.println("Move : Error symbol not found ");
                
        
       Card carddest =  null;     
        if (valid) {        
            
            Deck deckdest = stacks.get(dest);
            for (int i = 0 ; i < arrlist.size() ;i++) {
                deckdest.add(arrlist.get(i));
                //valid = cardsrc.getSymbol().equals(symbol);
                //System.out.println(cardsrc.getSymbol().equals(symbol)); 
            }
            
        }
       
       if (valid) {
            for (int i = 0 ; i < count ;i++) {
                    decksrc.remove(0);
           }
        }
            
            
            
    }
        
        
    

    /** 
     *  Moves one card onto each stack, or as many as are available
     */
    public void drawCards() {
        /* *** TO BE IMPLEMENTED IN ACTIVITY 5 *** */
        if (!isEmpty()) {
            Card card = cardPile.getTopStack(0);
            card.setFaceUp(true);
            System.out.println(card.toString());
        }
        
    }

    /**
     *  Returns true if all stacks and the draw pile are all empty
     */ 
    public boolean isEmpty() {
        /* *** TO BE IMPLEMENTED IN ACTIVITY 5 *** */
        
        if (cardPile.getSize() == 0) return true;
        
        for (Deck deck :  stacks) {
            if (deck.getSize() > 0) return false;
        } 
        return true;
    }

    /**
     *  If there is a run of A through K starting at the end of sourceStack
     *  then the run is removed from the game or placed into a completed
     *  stacks area.
     *  
     *  If there is not a run of A through K starting at the end of sourceStack
     *  then an invalid move message is displayed and the Board is not changed.
     */
    public void clear(int sourceStack) {
        /* *** TO BE IMPLEMENTED IN ACTIVITY 5 *** */
        
         for (int i = 0 ; i < numStacks ;i++) {
            Deck deck1 = stacks.get(i);
            Card card1 = (Card) deck1.getTopStack(0);
            
            
        }
    }

    /**
     * Prints the board to the terminal window by displaying the stacks, draw
     * pile, and done stacks (if you chose to have them)
     */
    public void printBoard() {
        /* *** TO BE IMPLEMENTED IN ACTIVITY 4 *** */
        
        System.out.println("Stacks " );
        int j = 0;
        for (Deck deck :  stacks) {
            ArrayList stackList = new ArrayList();
            if (deck.getSize() > 0) {
                j++;
                System.out.print("\n"+j+" "+":");
                System.out.print("[");
            }
            Card card = null;
            for (int i = deck.getSize() - 1 ; i >= 0 ;i--) {
                card = deck.getTopStack(i);
                //System.out.println("i ="+i+" card.getSymbol()" + card.getSymbol());
                String val = "X";
                if (card.isFaceUp()) val = card.getSymbol();
                
                if (i > 0)
                    System.out.print(val+",");
                else 
                    System.out.print(val+"]");
            }
            
        }
        
        Card card = null;
        if (this.cardPile != null) {
        int end = this.totalCards/2;
        cardPileList = new ArrayList();
        String val = "X";
        for (int i = 0 ; i < end ;i++) {
            card = this.cardPile.getTopStack(i);
            if (card.isFaceUp()) val = card.getSymbol();
            
            cardPileList.add(val);
        }
    
        System.out.println("\n Draw Pile:");
        System.out.println(Arrays.toString(cardPileList.toArray()));
    } else {
        System.out.println("\n Draw Pile:" +" []");
    }
        
    }
}