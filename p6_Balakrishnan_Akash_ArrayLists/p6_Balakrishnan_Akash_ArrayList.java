import java.util.*;

/**
 * Write a description of class p6_Balakrishnan_Akash_ArrayList here.
 *
 * @author (Akash Balakrishnan)
 * @version (a version number or a date)
 * 
 * Had issues  java.util.ConcurrentModificationException 
 * for remove from ArrayList
 * Used Iterator to solve it .
 * 
 */
public class p6_Balakrishnan_Akash_ArrayList
{
   public static void main(String[] args) {
       
       ArrayList<Integer> arrli = p6_Balakrishnan_Akash_ArrayList.randomList(7, 5, 12);
       p6_Balakrishnan_Akash_ArrayList.printList(arrli);
       
       arrli = p6_Balakrishnan_Akash_ArrayList.randomList(7, -10, 10);
       System.out.println("\n Before Negatives");
       printList(arrli);
       
       System.out.println("\n Remove Negatives");
       removeNegatives(arrli);
       printList(arrli);
       
       
       System.out.println("\n\n BEFORE");
       ArrayList<Integer> arrl1 = randomList(25, -5, 10);
       ArrayList<Integer> arrl2 = randomList(25, -5, 10);
       System.out.print("\n a=");printList(arrl1);
       System.out.print("\n b=");printList(arrl2);
       System.out.println("AFTER REMOVING NEGATIVES");
       System.out.print("\n a=");
       removeNegatives(arrl1);  //added negative check
       printList(arrl1);
       System.out.print("\n b="); 
       removeNegatives(arrl2); //added negative check
       printList(arrl2);
       arrli = p6_Balakrishnan_Akash_ArrayList.commonList(arrl1, arrl2);
       System.out.println("\n   COMMON LIST");
       printList(arrli);
    }
    
    public static void printList(ArrayList<Integer> arrList) {
        System.out.print("[");
        /*int len = arrList.size();
        for (int i = 0 ; i < len ;i++) {
            System.out.print(arrList.get(i));
            if (i < len-1) System.out.print(" ");
        }*/
        
        for (Integer item : arrList) {
            System.out.print(item+" ");
        }
        
        System.out.println("]");
        
    }
    
    /*
    n – The total number of random Integers to generate
    a – The lower bound of the random Integer range (inclusive)
    b – The upper bound of the random Integer range (inclusive)


    A new ArrayList of n <Integer> objects between a and b (inclusive)
     */
    public static ArrayList<Integer> randomList(int n, int a, int b) {
        ArrayList<Integer> arrli = new ArrayList<Integer>();
        Random r = new Random();
        for (int i = 0 ; i < n ;i++)
             arrli.add(r.nextInt((b - a) + 1) + a);

        
        return arrli;
    }
    
    
    public static void removeNegatives(ArrayList<Integer> arrList) {
        
        Iterator<Integer> iter = arrList.iterator();

        while (iter.hasNext()) {
          Integer val = iter.next();
        
          if (val < 0) {
            iter.remove();
          }
        }
       
       
    }
    
    
    public static ArrayList<Integer> commonList(ArrayList<Integer> arrList1, ArrayList<Integer> arrList2) {
        ArrayList<Integer> arrli = new ArrayList<Integer>();
        
        
        for (Integer val : arrList1) {
            if (arrList2.contains(val)) {
                if (!arrli.contains(val))
                    arrli.add(val);
            }
        }
        
        return arrli;
    }
    
}
