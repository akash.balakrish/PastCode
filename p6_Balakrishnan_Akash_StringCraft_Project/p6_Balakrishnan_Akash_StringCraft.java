import java.util.Random;
/**
 * Write a description of class p6_Balakrishnan_Akash_SpringCraft here.
 *
 * @author (Akash Bala)
 * @version (v1)
 * 
 * time taken : 4 hours 
 * Had difficulty in calculating the previous value 
 * 1 more than the previous letter in the run
 * finding the illegal moves and switching back the string
 * 
 */
public class p6_Balakrishnan_Akash_StringCraft
{
    // instance variables - replace the example below with your own
    private int stringLength;
    private String randomString;
    private StringCraft[] stringCraft;
    private int costVal;
    private int[] chars;
    private String cString;

    /**
     * Constructor for objects of class p6_Balakrishnan_Akash_StringCraft
     */
    public p6_Balakrishnan_Akash_StringCraft()
    {
        // initialise instance variables
        stringLength = 0;
        initialize();
    }

    
    /**
     * Constructor for objects of class p6_Balakrishnan_Akash_StringCraft
     */
    public p6_Balakrishnan_Akash_StringCraft(int len)
    {
        // initialise instance variables
        stringLength = len;
        initialize();
        
    }
    
    public void initialize() {
        
        stringCraft = new StringCraft[getStringLength() + 1];
        chars = new int[256];
    }
    
    
    public int getStringLength() {
        
        return stringLength;
    }
    
    
    public void setCurrentState(String str) {
        cString = str;
        
    }
    
    
    public String getCurrentState() {
        return cString;
    }
    
    public int getPoints() {
        int value = 0;
        for (int i = 0 ; i < getStringLength(); i++) {
            StringCraft strc = stringCraft[i];
            value += strc.getValue();
        }
        
        return value;
    }
    
    public void setCost(int cost) {
        
        costVal = cost;
    }
    
    public int getCost() {
        return costVal;
        
    }
    
    public int getScore() {
        
        return (getPoints() - getCost());
    }
    
    public void move(int mov1, int mov2) {
        StringCraft sc1 = stringCraft[mov1] ;
        StringCraft sc2 = stringCraft[mov2] ;
        
        stringCraft[mov1] = sc2;
        stringCraft[mov2] = sc1;
    }
    
    /**
     * An example of a method - replace this comment with your own
     *
     * @param  y  a sample parameter for a method
     * @return    the sum of x and y
     */
    public String getRandomString()
    {
        StringBuilder sb = new StringBuilder();
        String all = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int len = all.length();
        // put your code here
        int randomInt = 0;
        int i = 0;
        char ch = '\0';
        Random randomGenerator = new Random();
        randomInt = randomGenerator.nextInt(len-1);
        int value = 1;
        while (i < getStringLength()) {
            ch = all.charAt(randomInt);
            sb.append(ch);
            i++;
            randomInt = randomGenerator.nextInt(len-1);
        }
        return sb.toString();
        //return "ABDKFKHGZADNN";
    }
    
     public int getIntVal(char ch) {
         return Character.getNumericValue(ch);
     }
     
     
     public String processString(String str, boolean move) {
        StringBuilder sb = new StringBuilder();
        // put your code here
        int i = 0;
        char ch = '\0';
        int value = 1;
        if (!move) initialize();
        while (i < getStringLength()) {
            if (!move)
                ch = str.charAt(i);
            else
                ch = stringCraft[i].getCharacter();
                

            if (i > 0) {
                int previous = i-1;
                char chp = stringCraft[previous].getCharacter();
                if (ch == chp) 
                {
                    value = stringCraft[previous].getValue() + 2;
                } else {
                   // System.out.println(ch+"   "+chp+"   "+getIntVal(ch)+"   "+getIntVal(chp)+"   "+(getIntVal(ch) - getIntVal(chp)));
                    if ((getIntVal(ch) - getIntVal(chp)) == 1) 
                        value = chars[getIntVal(chp)] + 1;
                    else if (chars[getIntVal(ch) + 1] > 1)
                        value = chars[getIntVal(ch) + 1];
                    else value = 1;
                    //System.out.println("Value ==>"+value);
                }
            }
            StringCraft strc =  new StringCraft(ch, value);
            chars[getIntVal(ch)] = value;
            sb.append(ch);
            System.out.println("====================i= "+i+"  "+ch+"  value =>"+value);
            stringCraft[i] = strc;
            i++;
        }
        return sb.toString();
    }
    
    
    class StringCraft {
        char character;
        int posVal;
        
       StringCraft(char ch, int val) {
            character =  ch;
            posVal = val;
       }
       
       public char getCharacter() {
           return character;
        }
        
        public int getValue() {
            return posVal;
        }
    }
}
