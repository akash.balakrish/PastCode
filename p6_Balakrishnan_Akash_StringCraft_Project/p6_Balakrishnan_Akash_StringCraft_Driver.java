import java.util.Scanner; 

/**
 * Write a description of class p6_Balakrishnan_Akash_StringCraft_Driver here.
 *
 * @author (Akash Balakrishnan)
 * @version (a version number or a date)
 */
public class p6_Balakrishnan_Akash_StringCraft_Driver
{
    
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the string length ");
        int strLength = sc.nextInt(); 

        System.out.println("String length is "+ strLength);
        
        p6_Balakrishnan_Akash_SpringCraft sct = new p6_Balakrishnan_Akash_SpringCraft(strLength);
        System.out.println("Generates a random string of that length made up of only uppercase letters.=> "+sct.getRandomString());
        sct.processString(sct.getRandomString(), false);
        sct.setCurrentState(sct.getRandomString());
        sct.setCost(0);
        int current = sct.getScore();
        System.out.println(sct.getRandomString()+" is worth "+ current);
        System.out.println("Enter the indexes of the characters you would like to swap: ");
        int mov1 = 0;
        int mov2 = 0;
        int counter = 0;
        boolean invalid = false;
        int previous = 0;
        boolean illegal = false;
        
        while (!invalid) {
            java.util.Scanner sc1 = new Scanner(System.in).useDelimiter(" ");
                
            if (sc.hasNextInt()) {
                mov1 = sc.nextInt(); invalid = isValid(mov1, strLength);}  else invalid = true;
            if (sc.hasNextInt()) {   
                mov2 = sc.nextInt(); invalid = isValid(mov2, strLength);} else invalid = true;
            
            System.out.println("Invalid "+invalid);
            
            if (!invalid) {
                previous = current;
                sct.move(mov1, mov2);
                if (illegal) {sct.setCost(counter); illegal = false;}
                else sct.setCost(++counter); 
                String currentString = sct.processString(sct.getCurrentState(), true);
                current = sct.getScore();
                if (current < previous) {
                     System.out.println("That move is illegal because it would lower your score to "+current);
                     sct.processString(sct.getCurrentState(), false);
                     illegal = true;
                     //counter--;
                } else {
                    sct.setCurrentState(currentString);
                    System.out.println(currentString+" is worth "+ sct.getScore());
                }
               
                
            System.out.println("Enter the indexes of the characters you would like to swap: ");
            }
            
        }
        
        
        System.out.println("Game Over! Your final score is "+ sct.getScore() + " points.");
    }
    
    
    public static boolean  isValid(int mov, int len) {
        boolean invalid = false;
        if ((mov < 0) || (mov >= len)) invalid = true;
        else invalid = false;
        //System.out.println("isValid "+invalid);
        return invalid;
    }
    
}
