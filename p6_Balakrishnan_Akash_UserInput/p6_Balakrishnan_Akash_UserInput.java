import java.util.Scanner;

/**
 * Write a description of class p6_Balakrishnan_Akash_UserInput here.
 *
 * @author (Akash Balakrishnan)
 * @version (a version number or a date)
 */
public class p6_Balakrishnan_Akash_UserInput
{
    // instance variables - replace the example below with your own
    
    static final Scanner sc = new Scanner(System.in);
    
    public static void main(String[] args) {
       
        String val = readLine("Enter the user input");
        System.out.println("Output "+val);
        
        int intval = readInt("Enter enter an integer: ");
        System.out.println("Output Integer :"+intval);
        
        double doubleval = readDouble("Enter enter an decimal: ");
        System.out.println("Output double : "+doubleval);
        
        
        boolean boolval = readBoolean("Enter enter an boolean: ");
        System.out.println("Output boolean : "+boolval);
    }
    
    
    public static String readLine(String str) {
        System.out.print(str+" : ");
        String out = sc.nextLine();
        return out;
    }

    public static int readInt (String str) {
        System.out.print(str+" : ");
        boolean exit = false;
        int result = 0;
        String out = "";
        while (!exit) {
            out = sc.nextLine();
            
            
            try {
            result = Integer.valueOf(out.trim());
            exit = true;
           }catch(Exception e){   
               System.out.println("Invalid input. "+str);
               exit = false;
            }
    }

       return result; 
    }
    
    
      public static double readDouble(String str) {
        System.out.print(str+" : ");
        boolean exit = false;
        double result = 0;
        String out = "";
        while (!exit) {
            out = sc.nextLine();
            try {
            result = Double.valueOf(out.trim());
            exit = true;
           }catch(Exception e){   
               System.out.println("Invalid input."+str);
               exit = false;
            }
    }
       return result; 
    }
    
    
    public static boolean readBoolean(String str) {
        System.out.print(str+" : ");
        boolean exit = false;
        boolean result = false;
        String out = "";
        while (!exit) {
            out = sc.nextLine();
            try {
            //result = Boolean.valueOf(out.trim());
            result = parseBoolean(out);
            exit = true;
           }catch(Exception e){   
               System.out.println("Invalid input."+str);
               exit = false;
            }
    }
       return result; 
    }
    
    public static boolean parseBoolean(String str) throws IllegalArgumentException {
        str = str.toLowerCase();
        if ((str.equals("true"))|| (str.equals("yes")) || (str.equals("affirmative")))
            return true;
        else if ((str.equals("false")) || (str.equals("no") )|| (str.equals("negative")))
            return false;   
        else throw new IllegalArgumentException(str+"  Not a valid boolean parsed" );

    }
    
   
}
